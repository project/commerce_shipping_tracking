<?php

namespace Drupal\Tests\commerce_shipping_tracking\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group commerce shipping tracking
 */
class SettingsPageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_shipping',
    'commerce_shipping_tracking',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'access commerce shipping tracking settings',
      'access content',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/commerce/config/shipping_tracking');
    $this->assertSession()->pageTextContains('Commerce Shipping Tracking');
  }

}
