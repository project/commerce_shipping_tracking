<?php

namespace Drupal\commerce_shipping_tracking\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements shipping tracking form to be added to a block.
 */
class CommerceShippingTrackingForm extends FormBase {

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * CommerceShippingTrackingForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_shipping_tracking_block';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#theme'] = 'commerce_shipping_tracking';

    $form['order_number'] = [
      '#title' => $this->t('Order number'),
      '#type' => 'textfield',
      '#description' => $this->t('This information can be found in the email you received when you placed the order.'),
    ];

    $form['email'] = [
      '#title' => $this->t('E-mail used on order'),
      '#type' => 'email',
      '#description' => $this->t('The email used when the order was placed.'),
    ];

    $form['actions'] = [
      '#type' => 'submit',
      '#value' => $this->t('View information'),
      '#ajax' => [
        'callback' => '::getShipmentInfo',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Returns the shipping state machine name.
   */
  public function getShippingState($order) {
    $shipments = $this->entityTypeManager->getStorage('commerce_shipment')->loadByProperties(['order_id' => $order->id()]);
    if ($shipments) {
      $shipment = reset($shipments);
      return $shipment->getState()->value;
    }
    else {
      // In case there are no shipments such as a non shippable product.
      return NULL;
    }

  }

  /**
   * Returns an order based on the order number.
   */
  public function getOrder($order_number) {
    $orders = $this->entityTypeManager->getStorage('commerce_order')->loadByProperties(['order_number' => $order_number]);
    $order = reset($orders);
    return $order;
  }

  /**
   * Maps the configuration to a key value array to be used in the block output.
   */
  public function getStateMessages($config) {
    $state_messages = [];
    $state_list = explode(PHP_EOL, $config);
    foreach ($state_list as $state) {
      $state = explode("|", $state);
      $state_messages[$state[0]] = str_replace("\r", '', $state[1]);
    }
    return $state_messages;
  }

  /**
   * Builds the ajax response to return the shipment state.
   */
  public function getShipmentInfo(array $form, FormStateInterface $form_state) {
    // Get configuration.
    $config = $this->configFactory->get('commerce_shipping_tracking.settings');
    $state_message = $config->get('state_message');
    $error_message = $config->get('error_message');

    $ajax_response = new AjaxResponse();
    $values = $form_state->getValues();

    if (!empty($values['order_number']) && !empty($values['email'])) {
      // Get form values.
      $order_number = $values['order_number'];
      $email = $values['email'];
      $order = $this->getOrder($order_number);
      if ($order) {
        // Only call shipment if there's a valid order.
        $shipping_state_machine_name = $this->getShippingState($order);
      }
      if (!$order || $order->mail->value !== $email || !$shipping_state_machine_name) {
        // Returns error if the conditions are not met.
        $ajax_response->addCommand(new HtmlCommand('.error-message', $error_message));
        $ajax_response->addCommand(new HtmlCommand('.success-message', ''));
        $ajax_response->addCommand(new HtmlCommand('.result-state', ''));
        return $ajax_response;
      }

      // There is an order, the email is valid and the product is shippable.
      $state_messages = $this->getStateMessages($config->get("shipping_states"));

      $ajax_response->addCommand(new HtmlCommand('.success-message', $state_message));
      $ajax_response->addCommand(new HtmlCommand('.result-state', $state_messages[$shipping_state_machine_name]));
      $ajax_response->addCommand(new HtmlCommand('.error-message', ''));
      return $ajax_response;
    }
    else {
      // Handles error messaging when fields are not filled.
      $ajax_response->addCommand(new HtmlCommand('.error-message', $error_message));
      $ajax_response->addCommand(new HtmlCommand('.success-message', ''));
      $ajax_response->addCommand(new HtmlCommand('.result-state', ''));
      return $ajax_response;
    }

  }

}
