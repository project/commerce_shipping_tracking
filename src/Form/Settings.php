<?php

namespace Drupal\commerce_shipping_tracking\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure the Commerce Shipping Tracking settings.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_shipping_tracking_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_shipping_tracking.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get current configuration.
    $config = $this->config('commerce_shipping_tracking.settings');

    $form['shipping_states'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Shipping states and mapping'),
      '#default_value' => $config->get('shipping_states'),
      '#description' => $this->t('Write your configuration separated by a "|" in the form shipping_state|label. Eg: draft|Preparing'),

    ];

    $form['state_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State Message'),
      '#description' => $this->t('The message to be displayed indicating the order shipping state.'),
      '#default_value' => $config->get('state_message'),
      '#size' => 200,
    ];

    $form['error_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error Message'),
      '#description' => $this->t('The error message to be displayed in case the order or email was not found.'),
      '#default_value' => $config->get('error_message'),
      '#size' => 200,
    ];

    $form_state->setRebuild(TRUE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('commerce_shipping_tracking.settings')
      ->set('shipping_states', $form_state->getValue('shipping_states'))
      ->set('state_message', $form_state->getValue('state_message'))
      ->set('error_message', $form_state->getValue('error_message'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
