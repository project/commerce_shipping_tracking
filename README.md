## CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION
------------

The Commerce Shipping Tracking (commerce_shipping_tracking) module provides a
block for end users to check the status of their order. The module has a
configuration page where site administrators can choose which messages to
be displayed and to map the shipping states to labels to be shown to the user.


## REQUIREMENTS
------------

This module requires the commerce shipping module since it is designed to
complement it.


## INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


## CONFIGURATION
-------------

 * First navigate to

   Commerce » Configuration » Shipping » Order Tracking Settings

   * Map the machine names of the shipping workflow to the labels you want
     to be displayed;

   * Configure the success and error messages to be displayed;

   * Click Save Configuration.

* A block is available to display using Block Layout

   * Configure where you want the block to be displayed.

## MAINTAINERS
-----------

 * Current maintainers: [nsalves](https://www.drupal.org/u/nsalves)

This project has been sponsored by:
 * NTT DATA
    NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
    NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
    NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
    We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate
